#include<iostream>
using namespace std;
#define MAX 8

struct Antrian {
    int data[MAX];
    int head;
    int tail;
};

Antrian antre;
void create();
bool isEmpty();
bool isFull();
void enqueue(int);
int dequeue();
void tampil();

void menu(){
    int pilih;
    int tambahNilai;
    int hapusNilai;
    do{
        cout << "Pilihan :\n";
        cout << "1. Enqueue\n";
        cout << "2. Dequeue\n";
        cout << "3. Cetak\n";
        cout << "4. Clear\n";
        cout << "5. Exit\n";
        cout << "Masukkan pilihan Anda :\n";
        cin >> pilih;
        switch(pilih){
            case 1:
            cout << "Masukkan nilai :\t\n";
            cin >> tambahNilai;
            enqueue(tambahNilai);
            break;
            case 2:
            cout << "Mengeluarkan nilai :\t\n";
            cin >> hapusNilai;
            dequeue();
            break;
            case 3:
            cout << "Cetak nilai :\t\n";
            tampil();
            break;
            case 4:
            create();
            break;
            default:
            cout << "Exit";
            break;
        }
    }while(pilih !=5);
}

int main(){
    menu();
    return 0;
}

void create(){
    antre.head = antre.tail =-1;
}

bool isEmpty(){
    if(antre.tail == -1){
        return true;
    }
    else
    {
        return false;
    }
}

bool isFull(){
    if(antre.tail == MAX - 1){
        return true;
    }
    else
    {
        return false;
    }
}

void enqueue(int data){
    if(isEmpty()){
        antre.tail = 0;
        antre.data[antre.tail] = data;
        antre.head = data;
    }
    else
    {
        antre.tail++;
        antre.data[antre.tail] = data;
        antre.head = antre.data[antre.head];
    }
    cout << data << " dimasukkan ke antrian" << endl;
}

int dequeue(){
    int i;
    int e = antre.data[antre.head+1];
    for(i = antre.head;i < antre.tail;i++){
        antre.data[i] = antre.data[i+1];
    }
    for (i = antre.head+1; i < antre.tail; i++){
        cout << antre.data[i] << " ";
    }
    antre.tail--;
    cout << "Angka yang keluar adalah " << e << endl;
}

void tampil(){
    if(!isEmpty()){
        for(int i = antre.head+1;i <= antre.tail;i++){
            cout<<antre.data[i] << " ";
        }
        cout << endl;
    }
    else{
        cout<<"data kosong";
    }
}
